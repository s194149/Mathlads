


function Upload
global unfildata
global data
global HisData

disp('Make sure your data file is in the working directory!')
disp('Press enter to reset file data')
filename = input('Please enter file name: ', 's');

%code to check validity of the data format
if isempty(filename)
    clc
    data = [];
    unfildata = [];
    disp('DATA RESET')
elseif isfile(filename)
    unfildata = readmatrix(filename);
    if size(unfildata,2) == 3
        
        for k = 1:34
            fprintf('_')
        end
        
        fprintf('\nData format correct \n');
        %creating a columnvector consisting of logical ones and zeros accoring to the requirements given in the text.
        %tfil is temperature filter gfil is growthrate filter and bfil is bacteria filter
        
        tfil=unfildata(:,1)>10 & unfildata(:,1)<60;
        
        gfil=unfildata(:,2)>0;
        
        bfil = (unfildata(:,3) == 1 | unfildata(:,3) == 2 | unfildata(:,3) == 3 |  unfildata(:,3) ==4);
        %we use this to create a logical matrix where rows consisting of only valid
        %data are only ones, and rows with at least 1 invalid number become a zero
        
        fildata=[tfil,gfil,bfil];
        %Loop to decide whether a row is valid or not with logical ones and zeros
        for i=1:size(fildata,1)
            
            if sum(fildata(i,:))~=3
                fildata(i,:)=0;
                fprintf('Row %d is invalid and removed \n',i);
                
            end
        end
        
        %inserting actual data into our logical filtered data matrix
        fildata=unfildata.*fildata;
        
        %Removal of all rows only containing zeros, and restoring the matrix into
        %having 3 columns
        fildata=nonzeros(fildata);
        data=reshape(fildata,length(fildata)/3,3);
        HisData = data;
        disp('Data transfer complete')
    elseif size(unfildata,2) ~= 3 || isnan(unfildata)
        disp('Error 40: User error, check number of columns and whether document uploaded is from word')
    end
else
    disp('Error 404: File not found')
end

end
