function Stat_Choice

%Displays statistics. Pretty obvious

global unfildata
global data

%Checks whether there is a data uploaded or not
if isempty(unfildata)
    disp('Please upload a file')
elseif isempty(data)
    disp('You removed too much data')
else
    Stat_Choice = StatisticsOptions;
    %Displays and calculates statistics :). This is done by using 'mean'
    %and 'std' functions.
    while Stat_Choice ~= 8
        switch Stat_Choice
            case 1
                result = mean(data(:,1));
                fprintf('The mean temperature is: %.2f \n', result)
            case 2
                result = mean(data(:,2));
                fprintf('The mean growth rate is: %.2f \n', result)
            case 3
                result = std(data(:,1));
                fprintf('The standard deviation of temperature is: %.2f \n', result)
            case 4
                result = std(data(:,2));
                fprintf('The standard deviation of growth rate is: %.2f \n', result)
            case 5
                result = size(data,1);
                if result == 69
                    fprintf('The number of rows: %d, nice \n', result)  %:)
                else
                    fprintf('The number of rows: %d \n', result)
                end
            case 6
                I = find(data(:,1) < 20); %Sorting Index
                result = mean(data(I,2));
                fprintf('The mean cold growth rate is: %.2f \n', result)
            case 7
                I = find(data(:,1) > 50); %Sorting Index
                result = mean(data(I,2));
                fprintf('The mean hot growth rate is: %.2f \n', result)
        end
        Stat_Choice = StatisticsOptions;
    end
    if Stat_Choice == 8
        clc
    end
    
end
end

