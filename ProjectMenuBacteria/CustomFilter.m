function CustomFilter
global data;
global HisData;
global unfildata;

if isempty(unfildata)
    disp('Please upload a file')
else
    Custom_Choice = CustomFilterOptions;
    
    while Custom_Choice ~= 7
        switch Custom_Choice
            case 1
                %Code for the upper and lower limits filter
                tempL = input('Insert lower limit: ', 's');
                tempU = input('Insert upper limit: ', 's');
                
                L = str2num(tempL);
                U = str2num(tempU);
                
                if isempty(U) || isempty(L)
                    disp('Error: Input NaN')
                elseif L < 0 || U < 0
                    disp('Error: Negative input')
                else
                    %code to prevent eventual errors made by user settings, first
                    %if the lower limit is larger than upper limit we switch
                    %between the two
                    if L > U
                        temp = L;
                        L = U;
                        U = temp;
                        fprintf('Data was sent in incorrect order.\nChanging limits: Lower limit = %d & Upper limit: %d. \n', L, U)
                        %if no difference an error message is displayed
                        
                    end
                    if L == U
                        disp('Lower and upper temperature equal... you are better than this')
                        %if correct input the program continues
                    end
                    
                    if U > L
                        disp('New parameters set')
                        data(data(:,1)<L,:) = [];
                        data(data(:,1)>U,:) = [];
                        k = length(data(:,1));
                        fprintf('There are now %d compatible bacteria \n',k);
                        %if none of these relations are applicable to userinput an
                        %error message is displayed
                    end
                    
                end
                %rest of cases are used to remove a bacteria from the data, this
                %way the user can decide which ones to keep for personal use
                
            case 2
                %Remove Salmonella (1)
                data(data(:,3) == 1,:) = [];
                disp('Eliminated Salmonella')
            case 3
                %Remove Bacillus cereus (2)
                data(data(:,3) == 2,:) = [];
                disp('Bacillus cereus got destroyed')
            case 4
                %Remove Listeria
                data(data(:,3) == 3,:) = [];
                disp('Listeria became irrelevant')
            case 5
                %Remove brochothrix
                data(data(:,3) == 4,:) = [];
                disp('Brochothrix thermospacta is no more')
                
                %last case is a data reset button which reverts the used data to
                %the original data from the upload function
            case 6
                clc
                data = HisData;
                disp('DATA RESET')
        end
        Custom_Choice = CustomFilterOptions;
    end
    if Custom_Choice == 7
        clc
    end
end
end

