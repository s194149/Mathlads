
% menu options for statistics

function Stat_Choice = StatisticsOption

Stat_Choice = menu('Statistics','Mean Temperature', 'Mean Growth Rate', 'Std Temperature', 'Std Growth rate', 'Rows', 'Mean Cold Growth Rate', 'Mean Hot Growth Rate', 'Back');

end

