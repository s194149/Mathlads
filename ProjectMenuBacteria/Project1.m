clc

disp('This program analyzes data about bacteria and shows statistics and plots with the data.')
disp('There is a custom filter that lets the user sort data.')
disp('To exit the command window, enter an empty space. This will return the user to the menu')

global data
global unfildata
global HisData
data = [];
unfildata = [];

choice = Options;

% Menu selection
%Add intro text

while choice ~= 5
    switch choice
        case 1
            clc
            Upload;
        case 2
            clc
            CustomFilter;
        case 3
            clc
            Statistics;
        case 4
            clc
            plotfix;
    end
    
    choice = Options;
    clc
end

