clc

disp('This program analyzes data about bacteria and shows statistics and plots with the data.')
disp('There is a custom filter that lets the user sort data.')
disp('To exit the command window, enter an empty space. This will return the user to the menu')

choice = menu('Bacertia Analyser','Upload Data', 'Custom Filter', 'Statistics', 'Plot', 'Quit');

while choice ~= 5
    switch choice
        case 1
            clc
            
            disp('Make sure your data file is in the working directory!')
            disp('Press enter to reset file data')
            filename = input('Please enter file name: ', 's');
            
            %code to check validity of the data format
            if isempty(filename)
                clc
                data = [];
                unfildata = [];
                disp('DATA RESET')
            elseif isfile(filename)
                unfildata = readmatrix(filename);
                if size(unfildata,2) == 3
                    
                    for k = 1:34
                        fprintf('_')
                    end
                    
                    fprintf('\nData format correct \n');
                    %creating a columnvector consisting of logical ones and zeros accoring to the requirements given in the text.
                    %tfil is temperature filter gfil is growthrate filter and bfil is bacteria filter
                    
                    tfil=unfildata(:,1)>10 & unfildata(:,1)<60;
                    
                    gfil=unfildata(:,2)>0;
                    
                    bfil = (unfildata(:,3) == 1 | unfildata(:,3) == 2 | unfildata(:,3) == 3 |  unfildata(:,3) ==4);
                    %we use this to create a logical matrix where rows consisting of only valid
                    %data are only ones, and rows with at least 1 invalid number become a zero
                    
                    fildata=[tfil,gfil,bfil];
                    %Loop to decide whether a row is valid or not with logical ones and zeros
                    for i=1:size(fildata,1)
                        
                        if sum(fildata(i,:))~=3
                            fildata(i,:)=0;
                            fprintf('Row %d is invalid and removed \n',i);
                            
                        end
                    end
                    
                    %inserting actual data into our logical filtered data matrix
                    fildata=unfildata.*fildata;
                    
                    %Removal of all rows only containing zeros, and restoring the matrix into
                    %having 3 columns
                    fildata=nonzeros(fildata);
                    data=reshape(fildata,length(fildata)/3,3);
                    HisData = data;
                    disp('Data transfer complete')
                elseif size(unfildata,2) ~= 3 || isnan(unfildata)
                    disp('Error 40: User error, check number of columns and whether document uploaded is from word')
                end
            else
                disp('Error 404: File not found')
            end
        case 2
            clc
            
            
            if isempty(unfildata)
                disp('Please upload a file')
            else
                Custom_Choice = menu('Custom Filter', 'Lower & Upper Temperature', 'Remove Salmonella Enterica', 'Remove Bacillus Cereus', 'Remove Listeria', 'Remove Brochothrix Thermophacta', 'Revert to default','Back');
                while Custom_Choice ~= 7
                    switch Custom_Choice
                        case 1
                            %Code for the upper and lower limits filter
                            tempL = input('Insert lower limit: ', 's');
                            tempU = input('Insert upper limit: ', 's');
                            
                            L = str2num(tempL);
                            U = str2num(tempU);
                            
                            if isempty(U) || isempty(L)
                                disp('Error: Input NaN')
                            elseif L < 0 || U < 0
                                disp('Error: Negative input')
                            else
                                %code to prevent eventual errors made by user settings, first
                                %if the lower limit is larger than upper limit we switch
                                %between the two
                                if L > U
                                    temp = L;
                                    L = U;
                                    U = temp;
                                    fprintf('Data was sent in incorrect order.\nChanging limits: Lower limit = %d & Upper limit: %d. \n', L, U)
                                    %if no difference an error message is displayed
                                    
                                end
                                if L == U
                                    disp('Lower and upper temperature equal... you are better than this')
                                    %if correct input the program continues
                                end
                                
                                if U > L
                                    disp('New parameters set')
                                    data(data(:,1)<L,:) = [];
                                    data(data(:,1)>U,:) = [];
                                    k = length(data(:,1));
                                    fprintf('There are now %d compatible bacteria \n',k);
                                    %if none of these relations are applicable to userinput an
                                    %error message is displayed
                                end
                                
                            end
                            %rest of cases are used to remove a bacteria from the data, this
                            %way the user can decide which ones to keep for personal use
                            
                        case 2
                            %Remove Salmonella (1)
                            data(data(:,3) == 1,:) = [];
                            disp('Eliminated Salmonella')
                        case 3
                            %Remove Bacillus cereus (2)
                            data(data(:,3) == 2,:) = [];
                            disp('Bacillus cereus got destroyed')
                        case 4
                            %Remove Listeria
                            data(data(:,3) == 3,:) = [];
                            disp('Listeria became irrelevant')
                        case 5
                            %Remove brochothrix
                            data(data(:,3) == 4,:) = [];
                            disp('Brochothrix thermospacta is no more')
                            
                            %last case is a data reset button which reverts the used data to
                            %the original data from the upload function
                        case 6
                            clc
                            data = HisData;
                            disp('DATA RESET')
                    end
                    Custom_Choice = CustomFilterOptions;
                end
                if Custom_Choice == 7
                    clc
                end
            end
        case 3
            clc
            
            if isempty(unfildata)
                disp('Please upload a file')
            elseif isempty(data)
                disp('You removed too much data')
            else
                Stat_Choice = menu('Statistics','Mean Temperature', 'Mean Growth Rate', 'Std Temperature', 'Std Growth rate', 'Rows', 'Mean Cold Growth Rate', 'Mean Hot Growth Rate', 'Back');
                %Displays and calculates statistics :). This is done by using 'mean'
                %and 'std' functions.
                while Stat_Choice ~= 8
                    switch Stat_Choice
                        case 1
                            result = mean(data(:,1));
                            fprintf('The mean temperature is: %.2f \n', result)
                        case 2
                            result = mean(data(:,2));
                            fprintf('The mean growth rate is: %.2f \n', result)
                        case 3
                            result = std(data(:,1));
                            fprintf('The standard deviation of temperature is: %.2f \n', result)
                        case 4
                            result = std(data(:,2));
                            fprintf('The standard deviation of growth rate is: %.2f \n', result)
                        case 5
                            result = size(data,1);
                            if result == 69
                                fprintf('The number of rows: %d, nice \n', result)  %:)
                            else
                                fprintf('The number of rows: %d \n', result)
                            end
                        case 6
                            I = find(data(:,1) < 20); %Sorting Index
                            result = mean(data(I,2));
                            fprintf('The mean cold growth rate is: %.2f \n', result)
                        case 7
                            I = find(data(:,1) > 50); %Sorting Index
                            result = mean(data(I,2));
                            fprintf('The mean hot growth rate is: %.2f \n', result)
                    end
                    Stat_Choice = StatisticsOptions;
                end
                if Stat_Choice == 8
                    clc
                end
                
            end
        case 4
            clc
            if isempty(unfildata)
                disp('Please upload a file')
            elseif isempty(data)
                disp('You removed too much data')
            else
                
                %We start to define many variables which will be used in the followin
                %code
                M=data;
                M=sortrows(M,1);
                BakType=M(:, 3);
                N=length(BakType);
                BakType=reshape(BakType,1,N);
                Plot_Choice = menu('Plots', 'Number of Bacteria', 'Growth Rate by Temperature','Back');
                legstr=[];
                soejle=[];
                
                %Code that determines which bacteria is viable for plotting
                
                if sum(data(:,3)==1)>0
                    
                    legstr=[legstr,"Salmonella enterica(1)"];
                    soejle=[soejle,sum(BakType(:) == 1)];
                end
                
                if sum(data(:,3)==2)>0
                    legstr=[legstr,"Bacillus cereus(2)"];
                    soejle=[soejle,sum(BakType(:) == 2)];
                end
                
                if sum(data(:,3)==3)>0
                    legstr=[legstr,"Listeria(3)"];
                    soejle=[soejle,sum(BakType(:) == 3)];
                end
                
                if sum(data(:,3)==4)>0
                    legstr=[legstr,"Brochothrix thermosphacta(4)"];
                    soejle=[soejle,sum(BakType(:) == 4)];
                end
                
                
                while Plot_Choice ~= 3
                    switch Plot_Choice
                        case 1
                            %code for plot of number of the different type of bacterias, which also
                            %are sorted into colors, with code from https://se.mathworks.com/matlabcentral/answers/57719-color-individual-bar-with-different-colors-in-bar-plot
                            color=['r','g','b','k'];
                            figure
                            hold on
                            m=length(color);
                            for k=1:length(soejle)
                                i=mod(k-1,m);
                                i = i + 1;
                                h=bar(k,soejle(k));
                                set(h,'FaceColor',color(i));
                            end
                            
                            legend(legstr,'location', 'southoutside');
                            title('Amount of Bacteria');
                            xlabel('Bacteria types');
                            ylabel('Number of Bacteria');
                            
                        case 2
                            %Code for growth rate plot
                            %Bacteria 1
                            Temp=M(:, 1);
                            Temp1=nonzeros((BakType(:) == 1).*Temp);
                            N1=length(Temp1);
                            Temp1=reshape(Temp1,1,N1);
                            
                            Grow=M(:, 2);
                            Grow1=nonzeros((BakType(:) == 1).*Grow);
                            Grow1=reshape(Grow1,1,N1);
                            
                            %Bacteria 2
                            
                            Temp2=nonzeros((BakType(:) == 2).*Temp);
                            N2=length(Temp2);
                            Temp2=reshape(Temp2,1,N2);
                            Grow2=nonzeros((BakType(:) == 2).*Grow);
                            Grow2=reshape(Grow2,1,N2);
                            
                            %Bacteria 3
                            
                            Temp3=nonzeros((BakType(:) == 3).*Temp);
                            N3=length(Temp3);
                            Temp3=reshape(Temp3,1,N3);
                            Grow3=nonzeros((BakType(:) == 3).*Grow);
                            Grow3=reshape(Grow3,1,N3);
                            
                            %Bacteria 4
                            
                            Temp4=nonzeros((BakType(:) == 4).*Temp);
                            N4=length(Temp4);
                            Temp4=reshape(Temp4,1,N4);
                            Grow4=nonzeros((BakType(:) == 4).*Grow);
                            Grow4=reshape(Grow4,1,N4);
                            
                            %Growth rate plot
                            
                            figure
                            hold on
                            plot(Temp1,Grow1)
                            plot(Temp2,Grow2)
                            plot(Temp3,Grow3)
                            plot(Temp4,Grow4)
                            title('Growthrate by temperature for different Bacteria types');
                            xlabel('Temperature');
                            ylabel('Growthrate');
                            legend(legstr, 'location', 'southoutside')
                            grid on
                    end
                    Plot_Choice = menu('Plots', 'Number of Bacteria', 'Growth Rate by Temperature','Back');
                end
                if Plot_Choice == 3
                    clc
                end
            end
    end
    
    choice = Options;
    clc
end

