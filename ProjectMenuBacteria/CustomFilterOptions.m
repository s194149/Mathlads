function Custom_Choice = CustomFilterOptions

Custom_Choice = menu('Custom Filter', 'Lower & Upper Temperature', 'Remove Salmonella Enterica', 'Remove Bacillus Cereus', 'Remove Listeria', 'Remove Brochothrix Thermophacta', 'Revert to default','Back');

end