function Plot_Choice

global data;
global unfildata;

%code for determining whether data is viable to plot
if isempty(unfildata)
    disp('Please upload a file')
elseif isempty(data)
    disp('You removed too much data')
else
    
    %We start to define many variables which will be used in the followin
    %code
    M=data;
    M=sortrows(M,1);
    BakType=M(:, 3);
    N=length(BakType);
    BakType=reshape(BakType,1,N);
    Plot_Choice =PlotOptions;
    legstr=[];
    soejle=[];
    
    %Code that determines which bacteria is viable for plotting
    
    if sum(data(:,3)==1)>0
        
        legstr=[legstr,"Salmonella enterica(1)"];
        soejle=[soejle,sum(BakType(:) == 1)];
    end
    
    if sum(data(:,3)==2)>0
        legstr=[legstr,"Bacillus cereus(2)"];
        soejle=[soejle,sum(BakType(:) == 2)];
    end
    
    if sum(data(:,3)==3)>0
        legstr=[legstr,"Listeria(3)"];
        soejle=[soejle,sum(BakType(:) == 3)];
    end
    
    if sum(data(:,3)==4)>0
        legstr=[legstr,"Brochothrix thermosphacta(4)"];
        soejle=[soejle,sum(BakType(:) == 4)];
    end
    
    
    while Plot_Choice ~= 3
        switch Plot_Choice
            case 1
                %code for plot of number of the different type of bacterias, which also
                %are sorted into colors, with code from https://se.mathworks.com/matlabcentral/answers/57719-color-individual-bar-with-different-colors-in-bar-plot
                color=['r','g','b','k'];
                figure
                hold on
                m=length(color);
                for k=1:length(soejle)
                    i=mod(k-1,m);
                    i = i + 1;
                    h=bar(k,soejle(k));
                    set(h,'FaceColor',color(i));
                end
                
                
                legend(legstr,'location', 'southoutside');
                title('Amount of Bacteria');
                xlabel('Bacteria types');
                ylabel('Number of Bacteria');
                
            case 2
                %Code for growth rate plot
                %Bacteria 1
                Temp=M(:, 1);
                Temp1=nonzeros((BakType(:) == 1).*Temp);
                N1=length(Temp1);
                Temp1=reshape(Temp1,1,N1);
                
                Grow=M(:, 2);
                Grow1=nonzeros((BakType(:) == 1).*Grow);
                Grow1=reshape(Grow1,1,N1);
                
                %Bacteria 2
                
                Temp2=nonzeros((BakType(:) == 2).*Temp);
                N2=length(Temp2);
                Temp2=reshape(Temp2,1,N2);
                Grow2=nonzeros((BakType(:) == 2).*Grow);
                Grow2=reshape(Grow2,1,N2);
                
                %Bacteria 3
                
                Temp3=nonzeros((BakType(:) == 3).*Temp);
                N3=length(Temp3);
                Temp3=reshape(Temp3,1,N3);
                Grow3=nonzeros((BakType(:) == 3).*Grow);
                Grow3=reshape(Grow3,1,N3);
                
                %Bacteria 4
                
                Temp4=nonzeros((BakType(:) == 4).*Temp);
                N4=length(Temp4);
                Temp4=reshape(Temp4,1,N4);
                Grow4=nonzeros((BakType(:) == 4).*Grow);
                Grow4=reshape(Grow4,1,N4);
                
                %Growth rate plot
                
                
                figure
                hold on
                plot(Temp1,Grow1)
                plot(Temp2,Grow2)
                plot(Temp3,Grow3)
                plot(Temp4,Grow4)
                title('Growthrate by temperature for different Bacteria types');
                xlabel('Temperature');
                ylabel('Growthrate');
                legend(legstr, 'location', 'southoutside')
                grid on
        end
        Plot_Choice =PlotOptions;
    end
    if Plot_Choice == 3
        clc
    end
end
end