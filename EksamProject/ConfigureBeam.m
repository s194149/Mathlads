global tempL tempBS

Choice_CB = menu('Configure Beam', 'Beam length', 'Beam support', 'Back');

while Choice_CB ~= 3
    switch Choice_CB
        case 1
            
            tempL = input('Input beam length: ', 's');
            tempL = str2num(tempL);
            
            if length(tempL) ~= 1
                tempL = [];
                disp('Error: beam length is a scalar - beam length reset');
            else
                
                tempL = DataFilter(tempL, 'beam length', 1);
            end
            
        case 2
            
            Choice_BS = menu('Beam support', 'Both', 'Cantilever', 'Back');
            
            while Choice_BS ~= 3
                switch Choice_BS
                    case 1 %Both
                        tempBS = 0;
                        fprintf('Beam support is now: Both \n')
                    case 2 %Cantilever
                        tempBS = 1;
                        fprintf('Beam support is now: Cantilever \n')
                end
                Choice_BS = menu('Beam support', 'Both', 'Cantilever', 'Back');
            end
            
    end
    Choice_CB = menu('Configure Beam', 'Beam length', 'Beam support', 'Back');
end

function Q = DataFilter(x,str,k)
global tempL 
Q  = x;

if isempty(x)
    fprintf('Error in input - %s reset. \n',str)
    return
end

   if length(x(x<=0)) ~= 0 && k == 1
    fprintf('Error: %d elements(s) equal to or below 0. Removed from list. \n', length(x(x<=0)))
    x(x<=0) = [];
elseif length(x(x == 0)) ~= 0 && k == 0
    fprintf('Error: %d element(s) equal to 0. Removed from list. \n', length(x(x==0)))
    x(x == 0) = [];
    
end

if length(x(x>tempL)) ~= 0 && ~strcmp(str, 'load forces')
    fprintf('Error: %d element(s) greater than beam. Removed from list. \n',length(x(x>tempL)))
    x(x>tempL) = [];
end

if length(x) == 0
    fprintf('No valid input. \n')
else 
    fprintf('Valid input: ')
    fprintf('%g ', x)
    fprintf('\n')

end
end



