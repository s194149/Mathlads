%Configure load forces, load position & position

Choice_CL = menu('Configure loads', 'Load forces', 'Load position', 'Position', 'Back');
global tempL tempBS tempa tempW tempx
            
while Choice_CL ~= 4
    switch Choice_CL
        case 1 %Load forces
            
            tempW = input('Input load force: ', 's');
            tempW = str2num(tempW);
            tempW = DataFilter(tempW, 'load forces', 0);
            
        case 2 %Load position
            
            tempa = input('Input load position: ','s');
            tempa = str2num(tempa);
            tempa = DataFilter(tempa, 'load position', 1);
            
        case 3 %position
            tempx = input('Input positions: ','s');
            tempx = str2num(tempx);
            tempx = DataFilter(tempx, 'position', 0);
            
            
    end
    Choice_CL = menu('Configure loads', 'Load forces', 'Load position', 'Position', 'Back');
end

function Q = DataFilter(x,str,k)
global tempL
Q  = x;

if isempty(x)
    fprintf('Error in input - %s reset. \n',str)
    return
end

if length(x(x<=0)) ~= 0 && k == 1
    fprintf('Error: %d elements(s) equal to or below 0. Removed from list. \n', length(x(x<=0)))
    x(x<=0) = [];
    if length(unique(x)) ~= length(x)
        num = length(x)-length(unique(x));
        fprintf('Error: %d value(s) were equal. Removed from list. \n',num)
        x = unique(x);
    end
elseif length(x(x == 0)) ~= 0 && k == 0
    fprintf('Error: %d element(s) equal to 0. Removed from list. \n', length(x(x==0)))
    x(x == 0) = [];
    
end

if length(x(x>tempL)) ~= 0 && ~strcmp(str, 'load forces')
    fprintf('Error: %d element(s) greater than beam. Removed from list. \n',length(x(x>tempL)))
    x(x>tempL) = [];
end

if length(x) == 0
    fprintf('No valid input. \n')
else
    fprintf('Valid input: ')
    fprintf('%g ', x)
    fprintf('\n')
    
end
end

