global tempL tempBS tempa tempW tempx

%Creates a directory in the working directory called Savedfiles. 
%All the files generated in SaveData will be stored there 
if ~exist('SavedFiles', 'dir')
    mkdir SavedFiles
end

%Checks that input is given in configure loads
if  ~isempty(tempW) && ~isempty(tempa)
    
    %%
    %This part has the function that if you put load force on the same load
    %position then the load forces gets summed on the load position.
    %Also if positions are equal the function unique is used to remove
    %repeating elements of equal value. 
    %There is also error handling from user input. For example if there are
    %3 loaded positions and 1 loaded force.
    
    temp = [];
    temp2 = [];
    u = unique(tempa);
    w = tempW;
    a = tempa;
    
    if length(tempa) ~= length(u)
    
    for k = 1:length(u)
        
        temp = sum(w(a==u(k)));
        temp2(k) = temp;
        
    end
   
    end
    tempa = unique(tempa);
    tempx = unique(tempx);
  
    if length(temp2) ~= length(tempa) && ~isempty(temp2)
        disp('Error: dimensions of load forces and load positons are not equal')
        fprintf('Load position dimensions: %d, Load force dimensions: %d',size(tempa,2),size(tempW,2))
        return
    elseif ~isempty(temp2)
        z = abs(length(u)-length(w));
        tempW = temp2;
        fprintf('Error: there were %d positions that were equal elements. Their respective force has been summed on to a single element. \nLoad force is now given by: ', z)
        fprintf('%g ',tempW)
        fprintf('\n')
    end
 
    temp = [];
    temp2 = [];
  %%
    bool = true;
    while bool
        %%
        %This part handles the actual saving of the file after everything
        %has been filtered properly. The name of the file is written into
        %the variable datafile. Then the file is stored in SavedFiles
        %directory and the suffix '.txt' is given to make it a txt-file. 
        %There is error handling if the name already exists and a option to
        %overwrite the file. Also if there is no file name. 
        %The data is stored as a cell array because the rows arent of equal
        %length. Lastly if everything went correctly there is a line in the
        %command window that shows that the file is generated. 
        
        DataFile  = input('Name of save file: ', 's');
        SavedFile = ['./SavedFiles/',DataFile, '.txt'];
        
        if isfile(SavedFile)
            disp('A saved file with that name already exists. Do you want to overwrite?')
            OW = input ('Y/N: ', 's');
            OW = upper(OW);
            
            if strcmp('Y',OW)
                
            elseif strcmp('N',OW)
                disp('Okay type a new file name')
                
            elseif isempty(OW)
                bool = false;
                
            else
                disp('Error in input')
                
            end
            
        elseif isempty(DataFile)
            bool = false;
            disp('Error: no file name')
          
        end
        
        fid = fopen(SavedFile, 'wt');
        D = {tempW;tempa;tempL;tempBS;tempx};
        writecell(D,SavedFile);
        fclose(fid);
        disp('File succesfully saved')
        bool = false;
        %%
    end
else
    %%
    %If load forces or load positons are not yet configured it is displayed
    %in command window which variables havent been defined yet. 
    if isempty(tempW)
        disp('Please input load forces')
    end
    
    if isempty(tempa)
        disp('Please input load positions')
    end
  
    
end


