clc
%Intro text
fprintf('Welcome to Beam-Bender \nThis program gives you the user the ability to calculate or plot the deflection of any beam you choose \nYou will be able to set beamlength and support type as well as the loadforces and loadpositions \nTo define a beamlength enter a scalar, to define the load positions and or the load forces enter a row of numbers seperated by commas \nThe program will give you the options to save the specific situations under a folder and load the already saved situations \nThe plot can only be made after a file is loaded from the folder \nAs default the beamlength is equal to 10 m and the support type is "both" \nTo get started configure your beam or start adding load positions and forces \nTo exit the program press the "Quit" button')
%Defining start conditions. It is given that tempBS describes the beam
%support and that "both" = 0
global tempBS tempL tempx
tempL = 10;
tempBS = 0;
tempx = -1;

%Making menu
Choice = menu('Beam Bender','Configure beam', 'Configure loads', 'Save beam and loads','Load beam and data', 'Beam deflection', 'Beam superposition', 'Plot', 'Quit');

%When the last element (Quit) is chosen the program closes.
while Choice ~= 8
    switch Choice
        case 1
            clc
            ConfigureBeam;
        case 2
            clc 
            ConfigureLoads;
        case 3
            clc
            SaveData;
        case 4
            clc
            LoadData;
        case 5
            clc
            BeamDeflection;
        case 6
            clc
            BeamSuperposition;
        case 7
            clc
            BeamPlot;
    end
    %The reason for the second defintion of choice is that so when the
    %program exits the switch statement that the menu is loaded once again.
    %This means that the program can only be stopped by clicking quit
    Choice = menu('Beam Bender','Configure beam', 'Configure loads', 'Save beam and loads','Load beam and data', 'Beam deflection', 'Beam superposition', 'Plot', 'Quit');
end


clc


%{
Features to add:
-Add Deflection/Superposition to main menu
-Add start text/help 
-If error in load display text


%}
