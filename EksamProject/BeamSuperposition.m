function deflection=beamSuperposition(positions,beamLength,loadPositions,loadForces,beamSupport)
global beamLength loadPositions loadForces beamSupport positions
if ~isempty(beamLength) && ~isempty(loadPositions) && ~isempty(loadForces) && ~isempty(beamSupport) && ~isempty(positions)
%Defining constants and variables
    
I=0.001;
E=200*10^9;
x=positions;
L=beamLength;
a=loadPositions;
W=(-1)*loadForces;

%Determines how to calculate deflection by which string input is given
%to the function. the upper code calculates in case "both" and the other in 
%the case "cantilever"

if strcmp('both',beamSupport)
    for i=1:length(x)
        
            %This code calculates the deflection on a given position by
            %generating a vector which consists of each loadforces
            %contribution to deflection at a specific point
            
            ysup1=W(x(i)<a).*(L-a(x(i)<a))*x(i)/(6*E*I*L).*(L^2-x(i)^2-(L-a(x(i)<a)).^2);
       
            ysup2=W(x(i)>=a).*a(x(i)>=a)*(L-x(i))/(6*E*I*L).*(L^2-(L-x(i))^2-a(x(i)>=a).^2);
            
            %the sum of all the contribution then gives us the total
            %deflection
            
            y(i)=sum([ysup1,ysup2]);
    end
    deflection=y;
    disp('deflection of both calculated');
    disp(deflection)
elseif strcmp('cantilever',beamSupport)
    for i=1:length(x)
            ysup1=W(x(i)<a)*x(i)^2/(6*E*I).*(3*a(x(i)<a)-x(i));
            
            ysup2=W(x(i)>=a).*a(x(i)>=a).^2/(6*E*I).*(3*x(i)-a(x(i)>=a));
            y(i)=sum([ysup1,ysup2]);
    end
    deflection=y;
    disp('deflection of cantilever calculated');
    disp(deflection)
end

else
disp('Error all the data was not loaded')
end
end
