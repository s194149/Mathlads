global LoadData_Choice k FileList
InitailizeLoad()

while LoadData_Choice ~= k
    for i = 1:(k-2)
        switch LoadData_Choice
            case i
                FileLoader(i)
            case (k-1)
                Delete()
                break
                
        end
        
    end
    LoadData_Choice = menu('Loaded files', FileList);
end

function Delete()

%This function as the name implies gives the ability to delete the text
%files stored in SavedFiles. There is also the option to delete all files.
%There is used a dynamic menu which displays all files in SavedFiles along
%with the option to delete all and go back to previous menu.
%We were unable to update the menu while in menu of loaded files. The only
%way to get the an updated menu is to go the first menu and then go into
%loaded files menu.

global HisFileList k FileList

HisFileList(k) = {'Back'};
HisFileList(k-1) = {'Delete all'};

Delete_Choice = menu('Delete files', HisFileList);

while Delete_Choice ~= k
    for j = 1:(k-2)
        switch Delete_Choice
            case j %Delete Specific
                
                path = './SavedFiles/';
                DeleteFile = convertStringsToChars(FileList(j));
                DF = fullfile(path, DeleteFile);
                
                fprintf(1, 'Now deleting %s\n', DeleteFile);
                delete(DF);
                
            case k-1 %Delete All
                
                path = './SavedFiles/';
                DeleteFile = '*.txt';
                DF = fullfile(path, DeleteFile);
                delete(DF)
                disp('All files deleted')
                break
        end
    end
    Delete_Choice = menu('Delete files', HisFileList);
end


end

function InitailizeLoad()
global FileList LoadData_Choice k HisFileList
SavedFile = ['./SavedFiles/', '*.txt'];
LD = string(struct2cell(dir(SavedFile)));
FileList = {};
FileListN = [];
LoadedData = {};

for i = 1:size(LD,2)
    FileList = [FileList, LD(1,i)];
end



HisFileList = FileList;

k = size(FileList,2) + 2;
FileList(k) = {'Back'};
FileList(k-1) = {'Delete'};
if k == 2
    disp('No saved files')
    return
else

LoadData_Choice = menu('Loaded files', FileList);

end
end

function FileLoader(i)
global FileList

Name = convertStringsToChars(FileList(i));
SavedFileName = ['./SavedFiles/',Name ];
LoadedData = readcell(SavedFileName);


%Defining variables
global beamLength loadPositions loadForces beamSupport positions loadForce loadPosition
beamLength = [LoadedData{3,:}];
beamLength(isnan(beamLength)) = [];

beamSupport = [LoadedData{4,:}];
beamSupport(isnan(beamSupport)) = [];

loadForces = [LoadedData{1,:}];
loadForces(isnan(loadForces)) = [];

if length(loadForces)==1
loadForce=loadForces;
end

loadPositions = [LoadedData{2,:}];
loadPositions(isnan(loadPositions)) = [];

if length(loadPositions)==1
loadPosition=loadPositions;
end



if positions == -1
    positions = [];
else
positions = [LoadedData{5,:}];
positions(isnan(positions)) = [];
end

if beamSupport == 0;
    beamSupport = 'both';
else
    beamSupport = 'cantilever';
end
disp('File loaded succesfully')

end



