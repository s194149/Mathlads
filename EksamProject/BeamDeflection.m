function deflection=beamDeflection(positions,beamLength,loadPosition,loadForce,beamSupport)
global beamLength loadPosition loadForce beamSupport positions

if ~isempty(positions)
I=0.001;
E=200*10^9;
x=positions;
L=beamLength;
a=loadPosition;
W=(-1)*loadForce;

%Determines how to calculate deflection by which string input is given
%to the function. the upper code calculates in case "both" and the other in 
%the case "cantilever"

if strcmp('both',beamSupport)
    for i=1:length(x)
        %if statements to calculate deflection differently according to
        %the position which is being calculated compared to loadposition 
        
        if x(i)<a
            y(i)=W*(L-a)*x(i)/(6*E*I*L)*(L^2-x(i)^2-(L-a)^2);
        else
            y(i)=W*a*(L-x(i))/(6*E*I*L)*(L^2-(L-x(i))^2-a^2);
        end
    end
    deflection=y;
    disp('deflection of both calculated');
    disp(deflection)
    
elseif strcmp('cantilever',beamSupport)
    for i=1:length(x)
        if x(i)<a
            y(i)=W*x(i)^2/(6*E*I)*(3*a-x(i));
        else
            y(i)=W*a^2/(6*E*I)*(3*x(i)-a);
        end
    end
    deflection=y;
    disp('deflection of cantilever calculated');
    disp(deflection) 
end
else
disp('Error all the data was not loaded')
end
end
        
    
    

